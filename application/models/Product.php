<?php 
	class Product extends CI_Model{
		function get_products(){
		return $this->db->get('products');	
		}

		function get_products_by_param($param){
			$this->db->like('description', $param);
			$this->db->or_like('name', $param);
			return $this->db->get('products');
		}

		function get_products_by_param_pagination($param, $limit, $start){
			$this->db->like('description', $param);
			$this->db->or_like('name', $param);
			$query = $this->db->get('products');
			if ($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $data[] = $row;
	            }
	            return $data;
	        }
        	return false;
		}

  		function get_products_pagination($limit, $start){
	    $this->db->limit($limit, $start);
        $query = $this->db->get("products");
	        if ($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $data[] = $row;
	            }
	            return $data;
	        }
        return false;
		}

		function get_product_by_id($id){
		return $this->db->get_where("products", array('id'=>$id));
		}
		function delete_product_by_id($id){
			$this->db->delete('products', array('id'=>$id));
		}
		function add_product($data){
			$this->db->insert('products', $data);
		}
		function update_product($data, $id){
			$this->db->where('id', $id);
			$this->db->update('products', $data);
		}
		function get_categories(){
		return $this->db->get('category');
		}

		function get_category_by_name($name){
			$this->db->where('name', $name);
			$result = $this->db->get('category');
			$row = $result->row();
			return $row;
		}

		function get_category_name($id){
			$this->db->where('id', $id);
			$result = $this->db->get('category');
			$row = $result->row();
			return $row;
		}

		function get_products_by_date($datefrom, $dateto)
		{
			$this->db->where('created BETWEEN '.$datefrom.'%'.' AND '.$dateto.'%');
			return $this->db->get();
		}

		function get_products_by_date_pagination($datefrom, $dateto, $limit, $start)
		{
			$fromdate = "'".date("Y-m-d", strtotime($datefrom)).'%'."'";
			$todate =  "'".date("Y-m-d", strtotime($dateto)).'%'."'";
			$sql = "SELECT * FROM products WHERE created BETWEEN $fromdate AND $todate";
			$query = $this->db->query($sql);
	        if ($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $data[] = $row;
	            }
	            return $data;
	        }
        return false;
		}

		function get_config_errors(){
			return array(
	        array(
	                'field' => 'name',
	                'label' => 'Name',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => "<div class='alert alert-danger'>
					<button class='close' data-dismiss='alert'>
					<span aria-hidden='true'>&times;</span>
					</button><strong>¡Alerta! </strong>"."You must type the name."."</div>")
	        ),
	        array(
	                'field' => 'price',
	                'label' => 'Price',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => "<div class='alert alert-danger'>
					<button class='close' data-dismiss='alert'>
					<span aria-hidden='true'>&times;</span>
					</button><strong>¡Alerta! </strong>"."You must type the price."."</div>")
	        ),
	        array(
	                'field' => 'description',
	                'label' => 'Description',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => "<div class='alert alert-danger'>
					<button class='close' data-dismiss='alert'>
					<span aria-hidden='true'>&times;</span>
					</button><strong>¡Alerta! </strong>"."You must type the description."."</div>")
	        )
		);
		}
	}
?>