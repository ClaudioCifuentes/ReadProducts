<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Create</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="col-md-9 col-md-offset-1">
		<h1>Create Product</h1>
			<hr>
			<div class="jumbotron">
				<form action="<?php echo site_url('principal/save_product');?>" method="post">
					<div class="form-group">
					<label>Name:</label>
					<input type="text" class="form-control" name="name" 
					placeholder="Type the name" value="">
					</div>
					<?php echo form_error('name');?>
					<div class="form-group">
					<label>Price:</label>
					<input type="float" class="form-control" name="price" placeholder="Type the price" value="">			
					</div>
					<?php echo form_error('price');?> 
					<div class="form-group">
					<label>Description:</label>
					<input type="text" class="form-control" name="description" placeholder="Type the description" value="">
					</div>
					<?php echo form_error('description');?>
					<div class="form-group">
					<label for="sel1">Select list:</label>
  						<select class="form-control" name="select">
							<?php foreach($categories->result_array() as $row){?>
							<option value="<?php echo $row['name']?>">
								<?php echo $row['name'];?>
							</option>
							<?php }?>	
  						</select>
					</div>
					<div class="form-group" align="center">
					<input type="submit" class="btn btn-success" value="Create">
					</div>
				</form>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>	
</body>
</html>