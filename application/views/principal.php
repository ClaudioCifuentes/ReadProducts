<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Principal</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui-1.12.0/jquery-ui.min.css');?>">
</head>
<body>
	<br/>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
			<h1>Read Products</h1>
			<hr>
			<form class="form-inline" role="form" action="<?php echo site_url('principal/index');?>" method="post" >
					<div class="input-group">
		      				<input type="text" class="form-control" size="40px" placeholder="Type product name or description..." id="dataordescription" name="param">
		      				<span class="input-group-btn">
		        			<button class="btn btn-primary" type="submit" id="search1"><span class="glyphicon glyphicon-search"></span></button>
		      				</span>
		    		</div>
		    		<div class="input-group">
		      				<input type="text" class="form-control" id="datefrom" placeholder="Date from..." name="datefrom">
		    		</div>
		    		<div class="input-group">
		      				<input type="text" class="form-control" id="dateto" placeholder="Date to..." name="dateto">
		       				<span class="input-group-btn">
		        			<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
		      				</span>
		    		</div>
	  	  			<div class="form-group pull-right">
					<button class="btn btn-danger" type="button" id="delete_selected"><span class="glyphicon glyphicon-remove-circle"></span>Delete Selected</button>
					<a href="<?php echo site_url('principal/get_cvs');?>" class="btn btn-info"><span class="glyphicon glyphicon-save"></span>Export CVS</a>
					<a href="<?php echo site_url('principal/createProduct');?>" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>Create Product</a>
		    		</div>
			</form>	
			</div>	
		</div>
		<br/>
		<div class="row">
			<div class="col-md-12">
    			<table class="table table-bordered">
    				<thead>
      					<tr>
      					<th><input type="checkbox" id="cbmaster" value=""></th>
        				<th>Name</th>
        				<th>Price</th>
        				<th>Description</th>
        				<th>Category</th>
        				<th>Created</th>
        				<th>Actions</th>
      					</tr>
    				</thead>
    				<tbody>
    				<?php if(is_array($results)){?>
    					<?php foreach($results as $row){?>
							<tr>
								<td>
								<input type="checkbox" value="
								<?php echo $row->id;?>">
								</td>	
								<td><?php echo $row->name;?></td>
								<td><?php echo $row->price;?></td>
								<td><?php echo $row->description;?></td>
								<td><?php echo $this->Product->get_category_name($row->category_id)->name;?>
								</td>
								<td><?php echo $row->created?></td>	
								<td>
								<a href="<?php echo site_url('principal/edit_product/'.$row->id);?>" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span>Edit</a>
								<a href="<?php echo site_url('principal/delete_product/'.$row->id);?>" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Delete</a>
								</td>			
							</tr>	
						<?php }?>
					<?php }?>		
   					</tbody>
  				</table>
  			</div>	
		</div>
		<div class="row">
			<div class="col-md-9">
				<div id="pagination"><?php echo $links; ?></div>
			</div>	
			<div class="col-md-3">
				<div class="input-group">
      				<input type="text" class="form-control" size="40px" placeholder="Type page number" id="page">
      				<span class="input-group-btn">
        			<button class="btn btn-primary" id="btnpage">Go</button>
      				</span>
		    	</div>
			</div>	
		</div>
		<input type="hidden" id="site_url" value="<?php echo site_url();?>" />
		<input type="hidden" id="choice" value="<?php echo $choice?>" />
	</div>	
	<script src="<?php echo base_url('assets/js/jquery-3.1.0.min.js" type="text/javascript');?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-ui.min.js" type="text/javascript');?>"></script>	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/actions.js');?>" type="text/javascript"></script>
</body>
</html>