<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Product');
		$this->load->library('pagination');
	}

	public function index(){
		if($this->input->post('param')!=null){
			if($this->input->post('param')==''){
				redirect('principal/index');
			}
			else if($this->input->post('datefrom')!=null && $this->input->post('dateto')!=null){
				if($this->input->post('datefrom')!='' && $this->input->post('dateto')!=''){
					$num_rows= $this->Product->get_products_by_date($this->input->post('datefrom'), $this->input->post('dateto'));
				}
				else{
					redirect('principal/index');
				}
			}
			else{
			$param = $this->input->post('param');	
			$num_rows = $this->Product->get_products_by_param($param)->num_rows();
			}
		}
		else{
			$num_rows = $this->Product->get_products()->num_rows();
		}
		
	    $config = $this->get_pagination_config($num_rows);

    	$this->pagination->initialize($config);
	   	$page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;

	   	$data['choice'] = round($config['total_rows']/$config['per_page']);
	   	$data['links'] = $this->pagination->create_links();

	   	if($this->input->post('param')!=null){
	   		$data["results"] = $this->Product->get_products_by_param_pagination($param, $config["per_page"], $page);
			$data['links']='';
			$data['choice']='';
		}
		else if($this->input->post('datefrom')!=null && $this->input->post('dateto')!=null)
		{
			$data["results"] = $this->Product->get_products_by_date_pagination($this->input->post('datefrom'), $this->input->post('dateto'), $config["per_page"], $page);
			$data['links']='';
			$data['choice']='';
		}
		else{
			$data["results"] = $this->Product->get_products_pagination($config["per_page"], $page);
		}
		$this->load->view('principal', $data);
	}

	public function get_pagination_config($num_rows){
		$config['base_url'] = site_url('principal/index');
	    $config['total_rows'] = $num_rows;
	    $config['per_page'] = 5;
	    $config['num_links'] = round($config['total_rows']/$config['per_page']);
      	
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        return $config;
	}

	public function createProduct(){
		$categories= $this->Product->get_categories();
		$data['categories']= $categories;
		$this->load->view('create_product', $data);
	}

	public function save_product(){
		$config = $this->Product->get_config_errors();
		$this->form_validation->set_rules($config);

 		if($this->form_validation->run() == FALSE)
        {
                $this->load->view('create_product');
        }
        else
        {
        	$category_id = $this->Product->get_category_by_name($this->input->post('select'));
            $data = array(
					'name'=> $this->input->post('name'),
					'price'=> $this->input->post('price'),
					'description'=> $this->input->post('description'),
					'category_id' => $category_id->id,
					'created' => date("Y-m-d H:i:s")
					);
		$this->Product->add_product($data);
		redirect('principal/index');
		}
	}

	public function get_product_data($id){
		$product = $this->Product->get_product_by_id($id);
		$result = $product->row();
		$data['result'] = $result;
		$categories= $this->Product->get_categories();
		$data['categories']= $categories;
		return $data;
	}

	public function edit_product($id){
		$data = $this->get_product_data($id);
		$this->load->view('edit_product', $data);
	}

	public function update_product($id){
		$config = $this->Product->get_config_errors();
		$this->form_validation->set_rules($config);

 		if($this->form_validation->run() == FALSE)
        {
                $data = $this->get_product_data($id);
				$this->load->view('edit_product', $data);
        }
        else
        {
        	$category_id = $this->Product->get_category_by_name($this->input->post('select'));
            $data = array(
					'name'=> $this->input->post('name'),
					'price'=> $this->input->post('price'),
					'description'=> $this->input->post('description'),
					'category_id' => $category_id->id,
					'modified' => date("Y-m-d H:i:s")
					);
		$this->Product->update_product($data, $id);
		redirect('principal/index');
		}
	}


	public function delete_product($id){
		$this->Product->delete_product_by_id($id);
		redirect('principal/index');
	}

	public function delete_selected($id){
		header('Access-Control-Allow-Origin: *'); 
		$this->Product->delete_product_by_id($id);
		$mensaje = array('success'=>'success');
		echo json_encode($mensaje);
	}

	public function get_cvs(){
	    $this->load->dbutil();
	    $this->load->helper('file');
	    $this->load->helper('download');
	    $report = $this->Product->get_products();
	    $new_report = $this->dbutil->csv_from_result($report);
	    force_download(date('H:i:s').'CVS.cvs', $new_report);
	}
}
