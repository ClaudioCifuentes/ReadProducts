$('document').ready(function(){
	var site_url = $('#site_url').val();
	$('#dateto').datepicker();
	$('#datefrom').datepicker();
	$('#btnpage').on('click',function(){
 			var value = parseInt($('#page').val(),10);
 			var choice = parseInt($('#choice').val(), 10);
 			console.log(value);
 			console.log(choice);
	 			if(value!='' && !isNaN(value)){
	 				if(value==1){
	 				window.location.href =  site_url+"/principal/index";
	 				}
	 				else if(value<=(choice+1)){
	 					value= (value-1)*5;
	 					window.location.href = site_url +"/principal/index/"+value;
	 				}
	 				else{
	 				window.location.href =  site_url+"/principal/index";	
	 				}
	 			}
	});
	$('#cbmaster').click(function(){
		$('input:checkbox').not(this).prop('checked', this.checked);
	});	
	$('#delete_selected').click(function(){
		$('input[type=checkbox]:checked').each(function(){
	    var id = $(this).val();
	    $.ajax({
            url: site_url+'/principal/delete_selected/'+id,
            type:'POST',
            dataType: 'json',
            success: function(){
                window.location.href =  site_url+"/principal/index";
                }
            });
		});
	});
});