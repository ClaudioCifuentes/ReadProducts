Requerimientos de la aplicación:
1. CodeIgniter 3.1
2. PHP >=5.
3. MySQL o MariaDB >= 5.5
4. Servidor web Apache.
5. JQuery 3.
6. Bootstrap 3 (Mediante CDN).

Se garantiza el correcto funcionamiento de la aplicación en Linux, en Windows la función de PHP date() requiere configuración adicional.
Para ejecutar el proyecto en Windows es necesario tener instalado MySQL, Apache y PHP. O bien descargar un conjunto de herramientas de software
como XAMPP o MAMP.
En Linux el proceso es el siguiente:
- Instalación de apache.

sudo apt-get install apache2

Nota: Para iniciar/detener el servicio -> sudo service apache2 start/stop

- Instalación de PHP 7.

sudo apt-get install php7.0 libapache2-mod-php7.0 php7.0-cli php7.0-mysql

- Instalación de MySQL Server 5.7.

sudo apt-get install mysql-server

- Instalación de phpmyadmin.

sudo apt-get install phpmyadmin
- Agregar al archivo /etc/apache2/apache2.conf la líneal final del archivo:

Include /etc/phpmyadmin/apache.conf 

- Además instalar:

apt install php-gettext

La configuración de la conexión a MySQL se realiza dentro del archivo application/config/database.php, donde se colocan las 
credenciales para la conexión. Finalmente ejecutar el script que se encuentra en la carpeta sql para crear la base de datos y 
las tablas correrspondientes.

Para ejecutar el proyecto solo es necesario copiar todos los archivos dentro de una nueva carpeta llamada ReadProducts y colocar la carpeta
dentro del directorio raíz del servidor Apache. O bien colocarlo en la carpeta raíz de XAMPP (htdocs) o de algún otro servidor con soporte PHP.
La URL es: URL del servidor/ReadProducts/